const swaggerJsdoc = require('swagger-jsdoc');
const fs = require('fs');

const options = {
  definition: {
    openapi: '3.0.3',
    info: {
      title: 'Collective API',
      version: '1.1.0',
    },
    servers: [
      {url:'http://localhost:8082/api'}
    ],
    tags: [
      {
        name: "Posts",
        description: "Blog posts"
      },
      {
        name: "Comments",
        description: "User comments on posts"
      },
      {
        name: "Messages",
        description: "Email messages"
      },
      {
        name: "Todos",
        description: "Todo list"
      },
      {
        name: "Products",
        description: "Products available for purchase"
      },
      {
        name: "Items",
        description: "Items currently in the cart"
      }
    ]
  },
  apis: [
    './app/comments/routes.js',
    './app/posts/routes.js',
    './app/messages/routes.js',
    './app/todos/routes.js',
    './app/shopping-cart/routes.js'
  ]
};

const docs = swaggerJsdoc(options);
fs.writeFileSync('./openapi.json', JSON.stringify(docs, null, 2));

module.exports = docs;