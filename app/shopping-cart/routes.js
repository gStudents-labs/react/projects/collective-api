const express = require('express')
const router = express.Router()
const db = require('../../lib/db')

/**
 * @openapi
 * components:
 *   schemas:
 *     Product:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *         priceInCents:
 *           type: integer
 *           format: int32
 */

/**
 * @openapi
 * /products:
 *   get:
 *     tags:
 *       - Products
 *     summary: Get all products.
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref : '#/components/schemas/Product'
 */
router.get('/products', (req, res, next) => {
  const products = db.products.findAll()
  res.json(products)
})

router.get('/products/:id', (req, res, next) => {
  const product = db.products.find(req.params.id)
  res.json(product)
})

router.delete('/products/:id', (req, res, next) => {
  const product = db.products.find(req.params.id)
  if (product) {
    db.products.delete(req.params.id)
  }

  res.status(200)
  res.end()
})

/**
 * @openapi
 * components:
 *   schemas:
 *     Item:
 *       type: object
 *       properties:
 *         product_id:
 *           type: integer
 *           format: int32
 *         quantity:
 *           type: integer
 *           format: int32
 */

/**
 * @openapi
 * /items:
 *   get:
 *     tags:
 *       - Items
 *     summary: Get all items currently in the cart.
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref : '#/components/schemas/Item'
 */
router.get('/items', (req, res, next) => {
  const items = db.items.findAll()
  res.json(items)
})

/**
 * @openapi
 * /items:
 *   post:
 *     tags:
 *       - Items
 *     summary: Add a quantity of a product to the cart.
 *     requestBody:
 *       description: 'Add product entry to the cart.'
 *       content:
 *         application/json:
 *            schema:
 *               $ref : '#/components/schemas/Item'
 *     responses:
 *       200:
 *         description: Success. The product was added to the cart and item entry was created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref : '#/components/schemas/Item'
 */
router.post('/items', (req, res, next) => {
  const item = db.items.insert({ 
    quantity: Number(req.body.quantity), 
    product_id: Number(req.body.product_id) 
  })  
  res.json(item)
})

router.post('/products/:productId/items', (req, res, next) => {
  const productId = parseInt(req.params.productId, 10)
  const product = db.products.find(req.params.productId)
  const item = db.items.insert({ quantity: req.body.quantity, product_id: product.id })

  res.json(item)
})

/**
 * @openapi
 * /products/{productId}/items/{itemId}:
 *   delete:
 *     tags:
 *       - Items
 *     summary: Delete a cart item.
 *     parameters:
 *       - name: productId
 *         in: path
 *         description: Id of product to remove from the cart.
 *         required: true
 *         schema:
 *           type: integer
 *           format: int32
 *       - name: itemId
 *         in: path
 *         description: Id of item entry to remove from the cart.
 *         required: true
 *         schema:
 *           type: integer
 *           format: int32
 *     responses:
 *       200:
 *         description: Delete was successful.
 */
router.delete('/products/:productId/items/:itemId', (req, res, next) => {
  const item = db.items.find(req.params.itemId)
  if (item) {
    db.items.delete(req.params.itemId)
  }

  res.status(200)
  res.end()
})

module.exports = router
