const express = require('express')
const router = express.Router()
const db = require('../../lib/db')

/**
 * @openapi
 * components:
 *   schemas:
 *     Message:
 *       type: object
 *       properties:
 *         subject:
 *           type: string
 *         body:
 *           type: string
 *         read:
 *           type: boolean
 *         starred:
 *           type: boolean
 *         labels:
 *            type: array
 *            items:
 *              type: string
 */

/**
 * @openapi
 * components:
 *   schemas:
 *     MessageCommand:
 *       type: object
 *       properties:
 *         command:
 *           type: string
 *           choices: [star, read, addLabel, removeLabel, delete]
 *         messageIds:
 *           type: array
 *           items:
 *             type: integer
 *             format: int32
 *         starred:
 *           type: string
 *         read:
 *           type: boolean
 *         label:
 *           type: string
 */

/**
 * @openapi
 * /messages:
 *   get:
 *     tags:
 *       - Messages
 *     summary: Get all messages.
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref : '#/components/schemas/Message'
 */
router.get('/messages', (req, res, next) => {
  const messages = db.messages.findAll()
  res.json(messages)
})

router.get('/messages/:id', (req, res, next) => {
  const message = db.messages.find(req.params.id)
  res.json(message)
})

/**
 * @openapi
 * /messages:
 *   post:
 *     tags:
 *       - Messages
 *     summary: Save a message.
 *     requestBody:
 *       description: 'The message to save.'
 *       content:
 *         application/json:
 *            schema:
 *               $ref : '#/components/schemas/Message'
 *     responses:
 *       200:
 *         description: Success. The saved message will be returned.
 *         content:
 *           application/json:
 *             schema:
 *               $ref : '#/components/schemas/Message'
 */
router.post('/messages', (req, res, next) => {
  const message = db.messages.insert({
    subject: req.body.subject,
    body: req.body.body,
    read: false,
    starred: false,
    labels: [],
  })
  res.json(message)
})

/**
 * @openapi
 * /messages:
 *   patch:
 *     tags:
 *       - Messages
 *     summary: Modify or delete a message.
 *     requestBody:
 *       description: 'The messages to modify and how to modify them. The "star" command will FLIP the current starred value. The "read" command will SET the property to the given value.'
 *       content:
 *         application/json:
 *            schema:
 *               $ref : '#/components/schemas/MessageCommand'
 *            example:
 *               messageIds: [2,3,5]
 *               command: "addLabel"
 *               label: "cool-messages"
 *     responses:
 *       200:
 *         description: Success. The complete (updated) list of messages is returned.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref : '#/components/schemas/Message'
 */
router.patch('/messages', (req, res, next) => {
  console.log('req.body', req.body)
  db.messages.findAll(req.body.messageIds).forEach(message => {
    commands[req.body.command](message, req.body)
  })
  res.status(200)
  res.send(db.messages.findAll())
})

const commands = {
  star (message, cmd) {
    message.starred = !message.starred
  },

  delete (message, cmd) {
    db.messages.delete(message.id)
  },

  read (message, cmd) {
    message.read = cmd.read
  },

  addLabel (message, cmd) {
    if (!message.labels.includes(cmd.label)) {
      message.labels.push(cmd.label)
    }
  },

  removeLabel (message, cmd) {
    if (message.labels.includes(cmd.label)) {
      message.labels.splice(message.labels.indexOf(cmd.label), 1)
    }
  },
}

module.exports = router
